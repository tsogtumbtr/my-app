import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
  return (
    <nav className="bg-gray-800 text-white w-64 py-12">
      <div className="p-4 font-bold">Menu</div>
      <ul>
        <li className="p-4">
          <NavLink to="/" className={(navData) => (navData.isActive ? 'active-link' : 'none')}>
            Home Home
          </NavLink>
        </li>
        <li className="p-4">
          <NavLink
            to="/albums"
            className={(navData) => (navData.isActive ? 'active-link' : 'none')}>
            Albums
          </NavLink>
        </li>
        <li className="p-4">
          <NavLink to="/users" className={(navData) => (navData.isActive ? 'active-link' : 'none')}>
            Users
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
