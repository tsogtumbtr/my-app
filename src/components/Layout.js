import PropTypes from 'prop-types';

import React from 'react';
import Header from './Header';
import Navigation from './Navigation';

const Layout = ({ children }) => {
  return (
    <div className="flex h-screen bg-gray-100">
      <Navigation />
      <div className="flex-1 flex flex-col overflow-hidden">
        <Header />
        <main className="flex-1 overflow-x-hidden overflow-y-auto bg-gray-200">{children}</main>
      </div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.object.isRequired
};

export default Layout;
