import React from 'react';

const Header = () => {
  return (
    <header className="bg-white shadow">
      <div className="flex items-center justify-between p-4">
        <div className="text-xl font-semibold">Logo</div>
      </div>
    </header>
  );
};

export default Header;
