import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserForm from './UserForm';
import { Link } from 'react-router-dom';
import { createUser } from '../../redux/actions/userActions';

const AddUser = () => {
  const dispatch = useDispatch();
  const [submitStatus, setSubmitStatus] = useState(null);

  // Retrieve the loading state from Redux
  const loading = useSelector((state) => state.users.loading);
  const error = useSelector((state) => state.users.error);

  const onSubmit = (data) => {
    // Reset submit status
    setSubmitStatus(null);

    // Dispatch the createUser action with the form data
    dispatch(createUser(data))
      .then((response) => {
        // Handle success
        setSubmitStatus({ status: 'success', message: 'User created successfully' });
        console.log('User created successfully:', response.payload);
      })
      .catch((error) => {
        // Handle error
        setSubmitStatus({ status: 'error', message: 'Error creating user' });
        console.log('Error creating user:', error.message);
      });
  };

  const defaultValues = {
    name: '',
    username: '',
    email: '',
    address: {
      street: '',
      suite: '',
      city: '',
      zipcode: '',
      geo: {
        lat: '',
        lng: ''
      }
    },
    phone: '',
    website: '',
    company: {
      name: '',
      catchPhrase: '',
      bs: ''
    }
  };

  return (
    <div className="container mx-auto mt-8">
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Add User</h2>
        <Link to="/users" className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600">
          Back to Users
        </Link>
      </div>

      <div className="bg-white p-6 rounded-md shadow-md">
        {loading === 'pending' && (
          <div className="flex items-center justify-center py-2">
            <div className="animate-spin rounded-full h-8 w-8 border-t-2 border-blue-500 border-t-blue-500"></div>
          </div>
        )}
        {error && <p>Error: {error}</p>}

        {/* Display submit status */}
        {submitStatus && (
          <div className={`text-${submitStatus.status === 'success' ? 'blue' : 'red'}-500 mb-4`}>
            {submitStatus.message}
          </div>
        )}

        {/* Pass the onSubmit function to the AddUserForm */}
        <UserForm onSubmit={onSubmit} defaultValues={defaultValues} isAdd={true} />
      </div>
    </div>
  );
};

export default AddUser;
