import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import React from 'react';

const UserTableRow = ({ user }) => {
  return (
    <tr key={user.id}>
      <td className="border border-gray-300 px-4 py-2">{user.id}</td>
      <td className="border border-gray-300 px-4 py-2">{user.name}</td>
      <td className="border border-gray-300 px-4 py-2">{user.username}</td>
      <td className="border border-gray-300 px-4 py-2">{user.email}</td>
      <td className="border border-gray-300 px-4 py-2">{user.phone}</td>
      <td className="border border-gray-300 px-4 py-2">{user.website}</td>
      <td className="border border-gray-300 px-4 py-2 text-center">
        <Link to={`/users/${user.id}`} className="text-blue-500 hover:underline pr-2">
          View
        </Link>
      </td>
    </tr>
  );
};

UserTableRow.propTypes = {
  user: PropTypes.object.isRequired
};

export default UserTableRow;
