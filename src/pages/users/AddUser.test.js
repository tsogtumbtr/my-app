// AddUser.test.js
import React from 'react';
import { render, screen, waitFor, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import AddUser from './AddUser';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

const mockedUser = {
  name: 'John Doe',
  username: 'johndoe',
  email: 'john@yahoo.com',
  address: {
    street: '',
    suite: '',
    city: '',
    zipcode: '',
    geo: {
      lat: '',
      lng: ''
    }
  },
  phone: '123',
  website: 'https://www.yahoo.com.',
  company: {
    name: '',
    catchPhrase: '',
    bs: ''
  }
};

describe('AddUser component', () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      users: {
        user: {},
        loading: 'idle',
        error: null
      }
    });
  });

  test('submits the form and handles success response', async () => {
    // Mock the asynchronous action with redux-thunk support
    const dispatchMock = jest
      .fn()
      .mockImplementation(() => Promise.resolve({ payload: mockedUser }));
    store.dispatch = dispatchMock;

    render(
      <Provider store={store}>
        <Router>
          <AddUser />
        </Router>
      </Provider>
    );

    // Fill out the form
    const nameInput0 = screen.getByLabelText('Name:');
    const nameInput1 = screen.getByLabelText('Username:');
    const nameInput2 = screen.getByLabelText('Email:');
    const nameInput3 = screen.getByLabelText('Phone:');
    const nameInput4 = screen.getByLabelText('Website:');

    // eslint-disable-next-line
    act(() => {
      userEvent.type(nameInput0, mockedUser.name);
      userEvent.type(nameInput1, mockedUser.username);
      userEvent.type(nameInput2, mockedUser.email);
      userEvent.type(nameInput3, mockedUser.phone);
      userEvent.type(nameInput4, mockedUser.website);
    });

    // Submit the form
    // eslint-disable-next-line
    act(() => {
      userEvent.click(screen.getByRole('button', { name: /submit/i }));
    });

    await waitFor(() => {
      // Confirm the success message is displayed
      expect(screen.getByText(/User created successfully/i)).toBeInTheDocument();

      // eslint-disable-next-line
      expect(store.dispatch).toHaveBeenCalledTimes(1); // Check if the action is called once      
    });

    // Confirm that loading indicator is hidden
    expect(screen.queryByText(/loading/i)).toBeNull();

    // Confirm that no error message is displayed
    expect(screen.queryByText(/error/i)).toBeNull();
  });

  test('submits the form and handles error response', async () => {
    // Mock the asynchronous action with redux-thunk support
    const dispatchMock = jest
      .fn()
      .mockImplementation(() => Promise.reject({ message: 'Error creating user' }));
    store.dispatch = dispatchMock;

    render(
      <Provider store={store}>
        <Router>
          <AddUser />
        </Router>
      </Provider>
    );

    // Fill out the form
    const nameInput0 = screen.getByLabelText('Name:');
    const nameInput1 = screen.getByLabelText('Username:');
    const nameInput2 = screen.getByLabelText('Email:');
    const nameInput3 = screen.getByLabelText('Phone:');
    const nameInput4 = screen.getByLabelText('Website:');

    // eslint-disable-next-line
    act(() => {
      userEvent.type(nameInput0, mockedUser.name);
      userEvent.type(nameInput1, mockedUser.username);
      userEvent.type(nameInput2, mockedUser.email);
      userEvent.type(nameInput3, mockedUser.phone);
      userEvent.type(nameInput4, mockedUser.website);
    });

    // Submit the form
    // eslint-disable-next-line
    act(() => {
      userEvent.click(screen.getByRole('button', { name: /submit/i }));
    });

    await waitFor(() => {
      // Confirm the success message is displayed
      expect(screen.getByText(/Error creating user/i)).toBeInTheDocument();

      // eslint-disable-next-line
      expect(store.dispatch).toHaveBeenCalledTimes(1); // Check if the action is called once
    });

    // Confirm that loading indicator is hidden
    expect(screen.queryByText(/loading/i)).toBeNull();

    // Confirm that error message is displayed
    // eslint-disable-next-line
    expect(screen.queryByText(/error/i)).not.toBeNull();
  });
});
