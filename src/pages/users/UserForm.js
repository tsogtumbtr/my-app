import PropTypes from 'prop-types';
import React from 'react';
import { useForm, Controller } from 'react-hook-form';

const AddUserForm = ({ onSubmit, defaultValues, isAdd }) => {
  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm({ defaultValues });

  const validateName = (value) => {
    return value.trim() !== '' || 'Name is required';
  };

  const validateUsername = (value) => {
    return value.trim() !== '' || 'Username is required';
  };

  const validateEmail = (value) => {
    // A basic email validation pattern
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(value) || 'Invalid email address';
  };

  const validatePhone = (value) => {
    // A basic phone number validation pattern
    const phonePattern = /^[0-9-+]+$/;
    return phonePattern.test(value) || 'Invalid phone number';
  };

  const validateWebsite = (value) => {
    // A basic URL validation pattern
    const urlPattern = /^(ftp|http|https):\/\/[^ "]+$/;
    return urlPattern.test(value) || 'Invalid website URL, example: http://www.google.com';
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="max-w mx-auto mt-8 grid grid-cols-2 gap-4 px-16">
      <div className="mb-4">
        <label htmlFor="name" className="block text-sm font-semibold mb-1">
          Name:
        </label>
        <Controller
          name="name"
          control={control}
          rules={{ validate: validateName }}
          render={({ field }) => (
            <>
              <input
                {...field}
                type="text"
                id="name"
                aria-label="name"
                className={`w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500 ${
                  errors.name ? 'border-red-500' : ''
                }`}
              />
              {errors.name && <p className="text-red-500 text-xs mt-1">{errors.name.message}</p>}
            </>
          )}
        />
      </div>

      <div className="mb-4">
        <label htmlFor="username" className="block text-sm font-semibold mb-1">
          Username:
        </label>
        <Controller
          name="username"
          control={control}
          rules={{ validate: validateUsername }}
          render={({ field }) => (
            <>
              <input
                {...field}
                type="text"
                id="username"
                aria-label="username"
                className={`w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500 ${
                  errors.username ? 'border-red-500' : ''
                }`}
              />
              {errors.username && (
                <p className="text-red-500 text-xs mt-1">{errors.username.message}</p>
              )}
            </>
          )}
        />
      </div>

      <div className="mb-4">
        <label htmlFor="email" className="block text-sm font-semibold mb-1">
          Email:
        </label>
        <Controller
          name="email"
          control={control}
          rules={{ validate: validateEmail }}
          render={({ field }) => (
            <>
              <input
                {...field}
                type="email"
                id="email"
                aria-label="email"
                className={`w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500 ${
                  errors.email ? 'border-red-500' : ''
                }`}
              />
              {errors.email && <p className="text-red-500 text-xs mt-1">{errors.email.message}</p>}
            </>
          )}
        />
      </div>

      <div className="mb-4 col-span-2">
        <label htmlFor="address" className="block text-md font-bold mb-1 py-2">
          Address:
        </label>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor="street" className="block text-sm font-semibold mb-1">
              Street:
            </label>
            <Controller
              name="address.street"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="street"
                  aria-label="street"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="suite" className="block text-sm font-semibold mb-1">
              Suite:
            </label>
            <Controller
              name="address.suite"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="suite"
                  aria-label="suite"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="city" className="block text-sm font-semibold mb-1">
              City:
            </label>
            <Controller
              name="address.city"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="city"
                  aria-label="city"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="zipcode" className="block text-sm font-semibold mb-1">
              Zipcode:
            </label>
            <Controller
              name="address.zipcode"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="zipcode"
                  aria-label="zipcode"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="lat" className="block text-sm font-semibold mb-1">
              Geo Lat:
            </label>
            <Controller
              name="address.geo.lat"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="lat"
                  aria-label="lat"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="lng" className="block text-sm font-semibold mb-1">
              Geo Lng:
            </label>
            <Controller
              name="address.geo.lng"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="lng"
                  aria-label="lng"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label htmlFor="phone" className="block text-sm font-semibold mb-1">
          Phone:
        </label>
        <Controller
          name="phone"
          control={control}
          rules={{ validate: validatePhone }}
          render={({ field }) => (
            <>
              <input
                {...field}
                type="text"
                id="phone"
                aria-label="phone"
                className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
              />
              {errors.phone && <p className="text-red-500 text-xs mt-1">{errors.phone.message}</p>}
            </>
          )}
        />
      </div>

      <div className="mb-4">
        <label htmlFor="website" className="block text-sm font-semibold mb-1">
          Website:
        </label>
        <Controller
          name="website"
          control={control}
          rules={{ validate: validateWebsite }}
          render={({ field }) => (
            <>
              <input
                {...field}
                type="text"
                id="website"
                aria-label="website"
                className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
              />
              {errors.website && (
                <p className="text-red-500 text-xs mt-1">{errors.website.message}</p>
              )}
            </>
          )}
        />
      </div>

      <div className="mb-4 col-span-2">
        <label htmlFor="company" className="block text-md font-bold mb-1 py-2">
          Company:
        </label>
        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor="companyName" className="block text-sm font-semibold mb-1">
              Company Name:
            </label>
            <Controller
              name="company.name"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="companyName"
                  aria-label="companyName"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="catchPhrase" className="block text-sm font-semibold mb-1">
              Catch Phrase:
            </label>
            <Controller
              name="company.catchPhrase"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="catchPhrase"
                  aria-label="catchPhrase"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
          <div>
            <label htmlFor="business" className="block text-sm font-semibold mb-1">
              Business:
            </label>
            <Controller
              name="company.bs"
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  type="text"
                  id="business"
                  aria-label="business"
                  className="w-full px-3 py-2 border rounded-md focus:outline-none focus:border-blue-500"
                />
              )}
            />
          </div>
        </div>
      </div>

      {isAdd && (
        <div className="col-span-2 mt-4">
          <button
            type="submit"
            className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600">
            Submit
          </button>
        </div>
      )}
    </form>
  );
};

AddUserForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  defaultValues: PropTypes.object.isRequired,
  isAdd: PropTypes.bool.isRequired
};

export default AddUserForm;
