import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import UserForm from './UserForm';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { fetchUserById } from '../../redux/actions/userActions';

const ViewUser = () => {
  const { userId } = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    // Dispatch the fetchPhotosByAlbumId action when the component mounts
    dispatch(fetchUserById(userId));
  }, [dispatch, userId]);

  const user = useSelector((state) => state.users.user);
  const loading = useSelector((state) => state.users.loading);
  const error = useSelector((state) => state.users.error);

  return (
    <div className="container mx-auto mt-8">
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">View User</h2>
        <Link to="/users" className="px-4 py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600">
          Back to Users
        </Link>
      </div>

      <div className="bg-white p-6 rounded-md shadow-md">
        {loading === 'pending' && (
          <div className="flex items-center justify-center py-2">
            <div className="animate-spin rounded-full h-8 w-8 border-t-2 border-blue-500 border-t-blue-500"></div>
          </div>
        )}
        {error && <p>Error: {error}</p>}

        {/* Pass the onSubmit function to the ViewUserForm */}
        {user && user.id && <UserForm onSubmit={() => {}} defaultValues={user} isAdd={false} />}
      </div>
    </div>
  );
};

export default ViewUser;
