import PropTypes from 'prop-types';
import React from 'react';
import UserTableRow from './UserTableRow';
import TableHeaderCell from './TableHeaderCell';

const UserTable = ({ users, onSort, sortConfig }) => {
  return (
    <table className="min-w-full bg-white border border-gray-300">
      <thead>
        <tr>
          <TableHeaderCell label="id" onSort={() => onSort('id')} sortConfig={sortConfig} />
          <TableHeaderCell label="name" onSort={() => onSort('name')} sortConfig={sortConfig} />
          <TableHeaderCell
            label="username"
            onSort={() => onSort('username')}
            sortConfig={sortConfig}
          />
          <TableHeaderCell label="email" onSort={() => onSort('title')} sortConfig={sortConfig} />
          <TableHeaderCell label="phone" onSort={() => onSort('title')} sortConfig={sortConfig} />
          <TableHeaderCell label="website" onSort={() => onSort('title')} sortConfig={sortConfig} />
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <UserTableRow key={user.id} user={user} />
        ))}
      </tbody>
    </table>
  );
};

UserTable.propTypes = {
  users: PropTypes.array.isRequired,
  onSort: PropTypes.func.isRequired,
  sortConfig: PropTypes.object.isRequired
};

export default UserTable;
