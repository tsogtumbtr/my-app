import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import React from 'react';

const AlbumTableRow = ({ album }) => {
  return (
    <tr key={album.id}>
      <td className="border border-gray-300 px-4 py-2">{album.id}</td>
      <td className="border border-gray-300 px-4 py-2">{album.title}</td>
      <td className="border border-gray-300 px-4 py-2 text-center">
        <Link to={`/albums/${album.id}`}>view photos</Link>
      </td>
    </tr>
  );
};

AlbumTableRow.propTypes = {
  album: PropTypes.object.isRequired
};

export default AlbumTableRow;
