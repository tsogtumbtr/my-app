import React from 'react';
import PropTypes from 'prop-types';

const PhotoList = ({ photos }) => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
      {photos.map((photo) => (
        <div key={photo.id} className="overflow-hidden rounded-lg shadow-lg">
          <a href={photo.url} target="_blank" rel="noopener noreferrer">
            <img src={photo.thumbnailUrl} alt={photo.title} className="w-full h-48 object-cover" />
          </a>
          <div className="px-6 py-4">
            <p className="text-xl font-semibold mb-2">{photo.title}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

PhotoList.propTypes = {
  photos: PropTypes.array.isRequired
};

export default PhotoList;
