import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';

const TableHeaderCell = ({ label, onSort, sortConfig }) => {
  const isSorted = sortConfig.key === label;

  return (
    <th onClick={() => onSort(label)} className="py-2 px-4 cursor-pointer hover:bg-gray-100">
      {label}
      {isSorted && (
        <span className="ml-2">
          {sortConfig.direction === 'asc' ? (
            <FontAwesomeIcon icon={faSortUp} />
          ) : (
            <FontAwesomeIcon icon={faSortDown} />
          )}
        </span>
      )}
      {!isSorted && <FontAwesomeIcon icon={faSort} className="ml-2" />}
    </th>
  );
};

TableHeaderCell.propTypes = {
  label: PropTypes.string.isRequired,
  onSort: PropTypes.func.isRequired,
  sortConfig: PropTypes.object.isRequired
};

export default TableHeaderCell;
