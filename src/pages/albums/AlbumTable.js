import PropTypes from 'prop-types';
import React from 'react';
import AlbumTableRow from './AlbumTableRow';
import TableHeaderCell from './TableHeaderCell';

const AlbumTable = ({ albums, onSort, sortConfig }) => {
  return (
    <table className="min-w-full bg-white border border-gray-300">
      <thead>
        <tr>
          <TableHeaderCell label="id" onSort={() => onSort('id')} sortConfig={sortConfig} />
          <TableHeaderCell label="title" onSort={() => onSort('title')} sortConfig={sortConfig} />
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {albums.map((album) => (
          <AlbumTableRow key={album.id} album={album} />
        ))}
      </tbody>
    </table>
  );
};

AlbumTable.propTypes = {
  albums: PropTypes.array.isRequired,
  onSort: PropTypes.func.isRequired,
  sortConfig: PropTypes.object.isRequired
};

export default AlbumTable;
