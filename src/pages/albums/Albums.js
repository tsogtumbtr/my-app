import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAlbums } from '../../redux/slices/albumSlice';
import AlbumTable from './AlbumTable';

const Albums = () => {
  const dispatch = useDispatch();
  const albums = useSelector((state) => state.media.albums);
  const loading = useSelector((state) => state.media.loading);
  const error = useSelector((state) => state.media.error);

  const itemsPerPage = 10;
  const [currentPage, setCurrentPage] = useState(1);
  const [sortConfig, setSortConfig] = useState({ key: null, direction: 'asc' });

  useEffect(() => {
    dispatch(
      fetchAlbums({
        page: currentPage,
        perPage: itemsPerPage,
        sortKey: sortConfig.key,
        sortOrder: sortConfig.direction
      })
    );
  }, [dispatch, currentPage, sortConfig]);

  const paginate = (type) => {
    if (type === 'next') setCurrentPage(currentPage + 1);
    else if (currentPage > 0) setCurrentPage(currentPage - 1);
  };

  const handleSort = (key) => {
    let direction = 'asc';
    if (sortConfig.key === key && sortConfig.direction === 'asc') {
      direction = 'desc';
    }
    setSortConfig({ key, direction });
  };

  // Determine whether there are more pages
  const hasMorePages = albums.length === itemsPerPage;

  return (
    <div className="p-6">
      <h3 className="text-2xl font-bold mb-4">Albums</h3>
      <div>
        {loading === 'pending' && (
          <div className="flex items-center justify-center py-2">
            <div className="animate-spin rounded-full h-8 w-8 border-t-2 border-blue-500 border-t-blue-500"></div>
          </div>
        )}
        {error && <p>Error: {error}</p>}
        {albums.length > 0 && (
          <AlbumTable albums={albums} onSort={handleSort} sortConfig={sortConfig} />
        )}
        {hasMorePages && (
          <div className="flex justify-between mt-4">
            <button
              onClick={() => paginate('previous')}
              className={`px-3 py-1 ${
                currentPage === 1
                  ? 'bg-gray-300 text-gray-500 cursor-not-allowed'
                  : 'bg-blue-500 text-white'
              }`}
              disabled={currentPage === 1}>
              Previous Page
            </button>

            <button onClick={() => paginate('next')} className="px-3 py-1 bg-blue-500 text-white">
              Next Page
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Albums;
