import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPhotosByAlbumId, fetchAlbumById } from '../../redux/slices/albumSlice';
import { useParams, useNavigate } from 'react-router-dom';
import PhotoList from './PhotoList';

const Photos = () => {
  const { albumId } = useParams();
  const navigate = useNavigate();

  const dispatch = useDispatch();
  const photos = useSelector((state) => state.media.photos);
  const album = useSelector((state) => state.media.album);
  const isLoading = useSelector((state) => state.media.loading);
  const error = useSelector((state) => state.media.error);

  useEffect(() => {
    // Dispatch the fetchPhotosByAlbumId action when the component mounts
    dispatch(fetchPhotosByAlbumId(albumId));
  }, [dispatch, albumId]);

  useEffect(() => {
    // Dispatch the fetchPhotosByAlbumId action when the component mounts
    dispatch(fetchAlbumById(albumId));
  }, [dispatch, albumId]);

  const goBack = () => {
    navigate(-1);
  };

  if (isLoading === 'pending') {
    return (
      <div className="flex items-center justify-center py-2">
        <div className="animate-spin rounded-full h-8 w-8 border-t-2 border-blue-500 border-t-blue-500"></div>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <>
      <div className="p-6">
        <button
          onClick={goBack}
          className="absolute top-0 right-0 mt-4 mr-4 bg-blue-500 text-white px-4 py-2 rounded-md">
          Go Back
        </button>
        <h3 className="text-2xl font-bold mb-4">Album ID: {album.id}</h3>
        <h3 className="text-2xl font-bold mb-4">Title: {album.title}</h3>

        <PhotoList photos={photos} />
      </div>
    </>
  );
};

export default Photos;
