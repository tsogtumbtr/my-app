import './App.css';
import { Route, Routes } from 'react-router-dom';
import Layout from './components/Layout';
import Home from './pages/Home';
import Albums from './pages/albums/Albums';
import Photos from './pages/albums/Photos';
import Users from './pages/users/Users';
import AddUser from './pages/users/AddUser';
import ViewUser from './pages/users/ViewUser';

function App() {
  return (
    <>
      <Layout>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/albums" element={<Albums />} />
          <Route path="/albums/:albumId" element={<Photos />} />
          <Route path="/users" element={<Users />} />
          <Route path="/users/add" element={<AddUser />} />
          <Route path="/users/:userId" element={<ViewUser />} />
        </Routes>
      </Layout>
    </>
  );
}

export default App;
