// App.test.js
import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';

// Additional tests for specific routes can be added as needed
test('renders Home component for the home route', () => {
  render(
    <Router>
      <App />
    </Router>
  );

  // Test that the Home component is rendered for the home route
  expect(screen.getByText(/Welcome to Home Page/i)).toBeInTheDocument();
});
