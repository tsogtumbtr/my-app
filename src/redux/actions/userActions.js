import { createAsyncThunk } from '@reduxjs/toolkit';

const baseUrl = 'https://jsonplaceholder.typicode.com';

// Async action creator for fetching users
export const fetchUsers = createAsyncThunk(
  'users/fetchUsers',
  async ({ page = 1, perPage = 10, sortKey, sortOrder }) => {
    try {
      const queryParams = `?_page=${page}&_limit=${perPage}&_sort=${sortKey}&_order=${sortOrder}`;
      const response = await fetch(`${baseUrl}/users${queryParams}`);
      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  }
);

// Async action creator for fetching post by Id
export const fetchUserById = createAsyncThunk('users/fetchUserById', async (userId) => {
  try {
    const response = await fetch(`${baseUrl}/users/${userId}`);
    const data = await response.json();
    return data;
  } catch (error) {
    throw error;
  }
});

// Async action creator for creating a new post
export const createUser = createAsyncThunk('users/createUser', async (postData) => {
  try {
    const response = await fetch(`${baseUrl}/users`, {
      method: 'POST',
      body: JSON.stringify(postData),
      headers: {
        'Content-type': 'application/json'
      }
    });
    const data = await response.json();
    return data;
  } catch (error) {
    throw error;
  }
});
