import { createAsyncThunk } from '@reduxjs/toolkit';

const baseUrl = 'https://jsonplaceholder.typicode.com';

// Async action creator for fetching albums
export const fetchAlbums = createAsyncThunk(
  'albums/fetchAlbums',
  async ({ page = 1, perPage = 10, sortKey, sortOrder }) => {
    try {
      const queryParams = `?_page=${page}&_limit=${perPage}&_sort=${sortKey}&_order=${sortOrder}`;
      const response = await fetch(`${baseUrl}/albums${queryParams}`);
      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  }
);

// Async action creator for fetching album by Id
export const fetchAlbumById = createAsyncThunk('albums/fetchAlbumById', async (albumId) => {
  try {
    const response = await fetch(`${baseUrl}/albums/${albumId}`);
    const data = await response.json();
    return data;
  } catch (error) {
    throw error;
  }
});

// Async action creator for fetching a single album by ID
export const fetchPhotosByAlbumId = createAsyncThunk(
  'albums/fetchPhotosByAlbumId',
  async (albumId) => {
    try {
      const response = await fetch(`${baseUrl}/photos?albumId=${albumId}`);
      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  }
);
