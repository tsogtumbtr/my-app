import { createSlice } from '@reduxjs/toolkit';
import { fetchAlbums, fetchAlbumById, fetchPhotosByAlbumId } from '../actions/albumActions';

const albumsSlice = createSlice({
  name: 'albums',
  initialState: {
    albums: [],
    album: {},
    photos: [],
    loading: 'idle',
    error: null
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchAlbums.pending, (state) => {
        state.loading = 'pending';
      })
      .addCase(fetchAlbums.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.albums = action.payload;
      })
      .addCase(fetchAlbums.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      })
      // Add a new case for fetchPhotosByAlbumId
      .addCase(fetchAlbumById.pending, (state) => {
        state.album = {};
        state.loading = 'pending';
      })
      .addCase(fetchAlbumById.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.album = action.payload;
      })
      .addCase(fetchAlbumById.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      })
      // Add a new case for fetchPhotosByAlbumId
      .addCase(fetchPhotosByAlbumId.pending, (state) => {
        state.loading = 'pending';
      })
      .addCase(fetchPhotosByAlbumId.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.photos = action.payload;
      })
      .addCase(fetchPhotosByAlbumId.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      });
  }
});

export { fetchAlbums, fetchAlbumById, fetchPhotosByAlbumId };

export default albumsSlice.reducer;
