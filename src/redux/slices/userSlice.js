// src/redux/slices/usersSlice.js
import { createSlice } from '@reduxjs/toolkit';
import { fetchUsers, fetchUserById, createUser } from '../actions/userActions';

const usersSlice = createSlice({
  name: 'users',
  initialState: {
    users: [],
    user: {},
    loading: 'idle',
    error: null
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.loading = 'pending';
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.users = action.payload;
      })
      .addCase(fetchUsers.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      })
      .addCase(fetchUserById.pending, (state) => {
        state.user = {};
        state.loading = 'pending';
      })
      .addCase(fetchUserById.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.user = action.payload;
      })
      .addCase(fetchUserById.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      })
      .addCase(createUser.pending, (state, action) => {
        state.loading = 'pending';
      })
      .addCase(createUser.fulfilled, (state, action) => {
        state.loading = 'idle';
        state.user = action.payload;
      })
      .addCase(createUser.rejected, (state, action) => {
        state.loading = 'idle';
        state.error = action.error.message;
      });
  }
});

export { fetchUsers, fetchUserById, createUser };

export default usersSlice.reducer;
