import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';

import albumSlice from '../slices/albumSlice';
import userSlice from '../slices/userSlice';

const reducer = combineReducers({
  media: albumSlice,
  users: userSlice
});
const store = configureStore({
  reducer
});
export default store;
