In this project I've completed all the tasks, I've used React, Tailwind and Redux, Redux-Toolkit tools. Also added Eslint.

Developed node version - v18.18.0. For package manager - you can use npm or yarn.

## Available Scripts

In the project directory, you can run:


### `npm install`

It will install all the required packages in the system, need to run it first.


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
This project has 2 test files, and 3 tests. In AddUser.test.js file contains 
- submits the form and handles success response
- submits the form and handles error response test cases

### `npm lint:fix`

This command will fix the eslint issues.


Home page

![Alt Text](./images/1.png)

Task 1, Albums - when you've click on view photos, it will navigate to Photos page.

![Alt Text](./images/2.png)

The Photos page by AlbumId

![Alt Text](./images/3.png)

Task 2, Users Page

![Alt Text](./images/4.png)

When you click on View - It will navigate to viewUser page.

![Alt Text](./images/5.png)

In Add User page, added validation on Name, Username, Email, Phone and Website fields.

![Alt Text](./images/6.png)

Once you click on submit, it will notify status of response.

![Alt Text](./images/7.png)

Task 3.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
This project has 2 test files, and 3 tests. In AddUser.test.js file contains 
- submits the form and handles success response
- submits the form and handles error response test cases

![Alt Text](./images/8.png)


### Available Routes

path="/" element={<Home />} 
path="/albums" element={<Albums />} 
path="/albums/:albumId" element={<Photos />}  
path="/users" element={<Users />}  
path="/users/add" element={<AddUser />}  
path="/users/:userId" element={<ViewUser />}  



### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

