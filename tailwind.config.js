/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      green: {
        500: '#00ff00',
      },
      red: {
        500: '#ff0000',
      },
    },
  },
  plugins: [],
}

